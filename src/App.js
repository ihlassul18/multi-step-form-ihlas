import { Info, Plan, AddOns, Summary, Thanks } from "./pages";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Sidebar from "./components/Sidebar";
function App() {
  return (
    <div className="body">
      <div className="body-form">
        <BrowserRouter>
          <Sidebar />
          <Routes>
            <Route path="/" element={<Info />} />
            <Route path="/plan" element={<Plan />} />
            <Route path="/addOns" element={<AddOns />} />
            <Route path="/summary" element={<Summary />} />
            <Route path="/thanks" element={<Thanks />} />
          </Routes>
        </BrowserRouter>
      </div>
    </div>
  );
}

export default App;
