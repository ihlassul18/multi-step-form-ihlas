import React, { useEffect } from "react";

const Sidebar = () => {
  useEffect(() => {
    const curr = window.location.pathname;
    const links = document.querySelectorAll(".link");
    links.forEach((val) => {
      if (val.id === curr) {
        val.firstElementChild != null && val.firstElementChild.classList.add("active");
      } else {
        val.firstElementChild != null && val.firstElementChild.classList.remove("active");
      }
    });
  }, []);
  return (
    <div className="sidebar">
      <div className="sidebarList">
        <div id="/" className="link toInfo">
          <div className="num1">
            <p>
              <b>1</b>
            </p>
          </div>
          <div className="exp expInfo">
            <p className="step">STEP 1</p>
            <p>
              <b>YOUR INFO</b>
            </p>
          </div>
        </div>
        <div id="/plan" className="link toPlan">
          <div className="num2">
            <p>
              <b>2</b>
            </p>
          </div>
          <div className="exp expPlan">
            <p className="step">STEP 2</p>
            <p>
              <b>SELECT PLAN</b>
            </p>
          </div>
        </div>
        <div id="/addOns" className="link toAddOns">
          <div className="num3">
            <p>
              <b>3</b>
            </p>
          </div>
          <div className="exp expAddOns">
            <p className="step">STEP 3</p>
            <p>
              <b>ADD-ONS</b>
            </p>
          </div>
        </div>
        <div id="/summary" className="link toSummary">
          <div className="num4">
            <p>4</p>
          </div>
          <div className="exp expSummary">
            <p className="step">STEP 4</p>
            <p>
              <b>SUMMARY</b>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
