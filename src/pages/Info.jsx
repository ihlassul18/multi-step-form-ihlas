import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router";

const Info = () => {
  const [data, setData] = useState({
    name: "",
    email: "",
    phone: "",
  });
  const navigate = useNavigate();

  useEffect(() => {
    const input = document.querySelectorAll(".input");
    input.forEach((e) => {
      if (e.value !== "") {
        document.querySelector(`.${e.name}`).classList.add("none");
        e.classList.remove("required");
      }
    });
  }, [data]);
  useEffect(() => {
    if (localStorage.getItem("dataUser")) {
      const user = localStorage.getItem("dataUser");
      const User = JSON.parse(user);
      setData({ ...data, ...User });
    }
  }, [data]);

  const setInput = (e) => {
    const { name, value } = e.target;
    const input = { ...data, [name]: value };
    setData(input);
  };

  const dataHandle = (e) => {
    e.preventDefault();
    if (data.name && data.email && data.phone) {
      const dataObj = JSON.stringify(data);
      localStorage.setItem("dataUser", dataObj);
      navigate("/plan");
      window.location.reload();
    } else {
      const input = document.querySelectorAll(".input");
      input.forEach((e) => {
        if (e.value === "") {
          document.querySelector(`.${e.name}`).classList.remove("none");
          e.classList.add("required");
        }
      });
    }
  };

  return (
    <div className="info">
      <div className="formInfo">
        <div className="headForm">
          <h1>Personal Info</h1>
          <p>Please provide your name, email address, and phone number.</p>
        </div>
        <form onSubmit={dataHandle}>
          <div className="grup-input">
            <div className="label">
              <label htmlFor="name">Name</label>
              <label className="none name" htmlFor="name">
                This file is required
              </label>
            </div>
            <input className="input" type="text" id="name" name="name" value={data.name} onChange={(e) => setInput(e)} />
          </div>
          <div className="grup-input">
            <div className="label">
              <label htmlFor="email">Email address</label>
              <label className="none email" htmlFor="email">
                This file is required
              </label>
            </div>
            <input className="input" type="email" id="email" name="email" value={data.email} onChange={(e) => setInput(e)} />
          </div>
          <div className="grup-input">
            <div className="label">
              <label htmlFor="phone-number">Phone Number</label>
              <label className="none phone" htmlFor="phone-number">
                This file is required
              </label>
            </div>
            <input className="input" type="text" id="phone-number" name="phone" value={data.phone} onChange={(e) => setInput(e)} />
          </div>
          <div className="btnGrup">
            <button type="submit" className="btn">
              Next Step
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Info;
