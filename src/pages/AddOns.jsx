import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router";

const AddOns = () => {
  const [data, setData] = useState({});
  const [feeAddOns, setFeeAddOns] = useState({
    onlineService: "1/mo",
    largerStorage: "2/mo",
    customizableProfil: "2/mo",
  });
  const navigate = useNavigate();

  useEffect(() => {
    const dataPlan = JSON.parse(localStorage.getItem("dataPlan"));
    const dataAddOns = JSON.parse(localStorage.getItem("addOns"));
    const arrAddOns = dataAddOns ? Object.keys(dataAddOns) : [];
    const input = document.querySelectorAll("input");
    input.forEach((elem) => {
      if (arrAddOns.includes(elem.id)) {
        document.getElementById(`${elem.id}`).checked = true;
      }
    });
    if (dataPlan.time === "Yearly") {
      const feePerYear = {
        onlineService: "10/yr",
        largerStorage: "20/yr",
        customizableProfil: "20/yr",
      };
      setFeeAddOns({ ...feeAddOns, ...feePerYear });
    }
  }, []);

  const handleCheck = ({ e, val }) => {
    const { name, checked } = e.target;
    if (checked) {
      if (JSON.parse(localStorage.getItem("addOns")) != null && Object.keys(JSON.parse(localStorage.getItem("addOns"))).length !== 0) {
        const dataAddOns = JSON.parse(localStorage.getItem("addOns"));
        setData({ ...dataAddOns, [name]: val });
      } else {
        setData({ ...data, [name]: val });
      }
    } else {
      if (Object.keys(data).length === 0 && JSON.parse(localStorage.getItem("addOns")) != null) {
        const dataAddOns = JSON.parse(localStorage.getItem("addOns"));
        const dataCache = { ...dataAddOns };
        delete dataCache[name];
        setData({ ...dataCache });
      } else {
        const dataCache = { ...data };
        delete dataCache[name];
        setData({ ...dataCache });
      }
    }
  };
  const handleAddOns = () => {
    if (Object.keys(data).length !== 0) {
      localStorage.setItem("addOns", JSON.stringify(data));
    }
    navigate("/summary");
    window.location.reload();
  };
  return (
    <div className="addOns">
      <div className="headAddOns">
        <h1>Pick add-ons</h1>
        <p>Add-ons help enhance your gaming experience.</p>
      </div>
      <div className="bodyAddOns">
        <div className="bodyCheckBox">
          <div className="chekbox">
            <input type="checkbox" name="onlineService" onChange={(e) => handleCheck({ e, val: feeAddOns.onlineService })} id="onlineService" />
            <div className="expCheckBox">
              <p>
                <b>Online service</b>
              </p>
              <p>Access to multiplayer games</p>
            </div>
            <div className="fee">
              <p>+${feeAddOns.onlineService}</p>
            </div>
          </div>
          <div className="chekbox">
            <input type="checkbox" name="largerStorage" onChange={(e) => handleCheck({ e, val: feeAddOns.largerStorage })} id="largerStorage" />
            <div className="expCheckBox">
              <p>
                <b>Larger storage</b>
              </p>
              <p>Extra 1TB of cloud save</p>
            </div>
            <div className="fee">
              <p>+${feeAddOns.largerStorage}</p>
            </div>
          </div>
          <div className="chekbox">
            <input type="checkbox" name="customizableProfil" onChange={(e) => handleCheck({ e, val: feeAddOns.customizableProfil })} id="customizableProfil" />
            <div className="expCheckBox">
              <p>
                <b>Customizable Profil</b>
              </p>
              <p>Custom them on your profile</p>
            </div>
            <div className="fee">
              <p>+${feeAddOns.customizableProfil}</p>
            </div>
          </div>
        </div>
        <div className="btnGrup">
          <div
            className="btnGoBack"
            onClick={() => {
              navigate("/plan");
              window.location.reload();
            }}
          >
            <p>Go Back</p>
          </div>
          <button className="btn" onClick={handleAddOns}>
            Next Step
          </button>
        </div>
      </div>
    </div>
  );
};

export default AddOns;
