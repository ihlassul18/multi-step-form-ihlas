import React, { useEffect, useState } from "react";
import symArcade from "../components/assets/images/icon-arcade.svg";
import symAdvance from "../components/assets/images/icon-advanced.svg";
import symPro from "../components/assets/images/icon-pro.svg";
import { useNavigate } from "react-router";

const Plan = () => {
  const [data, setData] = useState({
    plan: [],
    time: "Monthly",
  });

  const [infoPlan, setInfoPlan] = useState({
    Arcade: "9/mo",
    Advanced: "12/mo",
    Pro: "15/mo",
  });

  const [toggle, setToggle] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    if (localStorage.getItem("dataPlan")) {
      const dataPlan = JSON.parse(localStorage.getItem("dataPlan"));
      document.getElementById(`${dataPlan.plan[0]}`)?.classList.add("selected");
      setData({ ...data, ...dataPlan });
      if (dataPlan.time === "Yearly") {
        document.querySelector(".switchBtn").style.setProperty("--move", "translateX(1.6rem)");
        document.querySelector(".year").classList.add("color");
        document.querySelector(".month").classList.remove("color");
        const caption = document.querySelectorAll(".caption");
        caption.forEach((elem) => {
          elem.innerHTML = "2 months free";
        });
        const feePerYear = {
          Arcade: `90/yr`,
          Advanced: `120/yr`,
          Pro: `150/yr`,
        };
        setInfoPlan({ ...infoPlan, ...feePerYear });
      }
    }
  }, []);

  const toggleSwitch = () => {
    const caption = document.querySelectorAll(".caption");
    setToggle(!toggle);
    localStorage.removeItem("addOns");
    localStorage.removeItem("dataPlan");
    document.querySelectorAll(".cardPlan").forEach((item) => item.classList.remove("selected"));
    if (toggle) {
      document.querySelector(".switchBtn").style.setProperty("--move", "translateX(1.6rem)");
      document.querySelector(".year").classList.add("color");
      document.querySelector(".month").classList.remove("color");
      caption.forEach((elem) => {
        elem.innerHTML = "2 months free";
      });
      const feePerYear = {
        Arcade: `90/yr`,
        Advanced: `120/yr`,
        Pro: `150/yr`,
      };
      setData({ ...data, time: "Yearly" });
      setInfoPlan({ ...infoPlan, ...feePerYear });
    } else {
      document.querySelector(".switchBtn").style.removeProperty("--move");
      document.querySelector(".year").classList.remove("color");
      document.querySelector(".month").classList.add("color");
      const feePerMonth = {
        Arcade: "9/mo",
        Advanced: "12/mo",
        Pro: "15/mo",
      };
      caption.forEach((elem) => {
        elem.innerHTML = "";
      });
      setData({ ...data, time: "Monthly" });
      setInfoPlan({ ...infoPlan, ...feePerMonth });
    }
  };

  const planSelected = ({ e, val }) => {
    localStorage.removeItem("addOns");
    const namePlan = e.currentTarget.id;
    setData({ ...data, plan: [namePlan, val] });
    const plan = document.querySelectorAll(".cardPlan");
    plan.forEach((element) => {
      if (element.id === namePlan) {
        element.classList.add("selected");
      } else {
        element.classList.remove("selected");
      }
    });
  };

  const handlePlan = () => {
    if (data.plan.length !== 0 && data.time) {
      const dataPlan = JSON.stringify(data);
      localStorage.setItem("dataPlan", dataPlan);
      localStorage.removeItem("addOns");
      navigate("/addOns");
      window.location.reload();
    }
  };
  return (
    <div className="plan">
      <div className="headPlan">
        <h1>Select your plan</h1>
        <p>You have the option of monthly or yearly billing</p>
      </div>
      <div className="bodyPlan">
        <div className="selectBodyPlan">
          <div className="cardPlan" id="Arcade" onClick={(e) => planSelected({ e, val: infoPlan.Arcade })}>
            <div className="imgPlan">
              <img src={symArcade} alt="arcade" />
            </div>
            <div className="infPlan">
              <p>
                <b>Arcade</b>
              </p>
              <p>${infoPlan.Arcade}</p>
              <p className="caption"></p>
            </div>
          </div>
          <div className="cardPlan" id="Advanced" onClick={(e) => planSelected({ e, val: infoPlan.Advanced })}>
            <div className="imgPlan">
              <img src={symAdvance} alt="advanced" />
            </div>
            <div className="infPlan">
              <p>
                <b>Advanced</b>
              </p>
              <p>${infoPlan.Advanced}</p>
              <p className="caption"></p>
            </div>
          </div>
          <div className="cardPlan" id="Pro" onClick={(e) => planSelected({ e, val: infoPlan.Pro })}>
            <div className="imgPlan">
              <img src={symPro} alt="pro" />
            </div>
            <div className="infPlan">
              <p>
                <b>Pro</b>
              </p>
              <p>${infoPlan.Pro}</p>
              <p className="caption"></p>
            </div>
          </div>
        </div>
        <div className="selectTime">
          <div className="month color">
            <p>Monthly</p>
          </div>
          <div className="switchBtn" onClick={toggleSwitch}></div>
          <div className="year">
            <p>Yearly</p>
          </div>
        </div>
        <div className="btnGrup">
          <div
            className="btnGoBack"
            onClick={() => {
              navigate("/");
              window.location.reload();
            }}
          >
            <p>Go Back</p>
          </div>
          <button className="btn" onClick={handlePlan}>
            Next Step
          </button>
        </div>
      </div>
    </div>
  );
};

export default Plan;
