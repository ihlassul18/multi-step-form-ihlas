import React, { useEffect } from "react";
import { useNavigate } from "react-router";
import symThanks from "../components/assets/images/icon-thank-you.svg";

const Thanks = () => {
  const navigate = useNavigate();
  useEffect(() => {
    setTimeout(() => {
      navigate("/");
      window.location.reload();
    }, 5000);
  }, [navigate]);
  return (
    <div className="thanks">
      <div className="bodyThanks">
        <div className="img">
          <img src={symThanks} alt="thanks" />
        </div>
        <div className="main">
          <h1>Thank you!</h1>
          <p>Thanks for confirming your subscription! we hope you have fun using our platform. if you ever need support, please feel free to email us at support@loremgaming.com</p>
        </div>
      </div>
    </div>
  );
};

export default Thanks;
