import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router";

const Summary = () => {
  const [plan, setPlan] = useState(null);
  const [addOns, setAddOns] = useState(null);
  const [total, setTotal] = useState(0);
  const [time, setTime] = useState("");
  const navigate = useNavigate();

  useEffect(() => {
    let total = 0;
    setPlan(JSON.parse(localStorage.getItem("dataPlan")));
    setTime(JSON.parse(localStorage.getItem("dataPlan")).plan[1].slice(-2));
    if (localStorage.getItem("addOns")) {
      setAddOns(JSON.parse(localStorage.getItem("addOns")));
      Object.values(JSON.parse(localStorage.getItem("addOns"))).map((num) => (total += parseInt(num)));
    }
    total += parseInt(JSON.parse(localStorage.getItem("dataPlan")).plan[1]);
    setTotal(total);
  }, []);

  const handleSubmit = () => {
    localStorage.clear();
    navigate("/thanks");
  };
  return (
    <div className="summary">
      <div className="headSummary">
        <h1>Finishing up</h1>
        <p>Double-check everything looks OK before confirming </p>
      </div>
      <div className="bodySummary">
        <div className="prevSummary">
          <div className="Plan">
            <div className="expPlan">
              <p>
                <b>
                  {plan != null && plan.plan[0]}({plan != null && plan.time})
                </b>
              </p>
              <div
                className="change"
                onClick={() => {
                  navigate("/plan");
                  window.location.reload();
                }}
              >
                change
              </div>
            </div>
            <p className="feePlan">${plan != null && plan.plan[1]}</p>
          </div>
          <div className="AddOns">
            {addOns != null &&
              Object.entries(addOns).map((data, index) => (
                <div className="dispAddOns" key={index}>
                  <div className="expAddOns">
                    <p>{data[0] === "onlineService" ? "Online service" : data[0] === "largerStorage" ? "Larger storage" : "Customizable profil"}</p>
                  </div>
                  <p className="feeAddOns">+${data[1]}</p>
                </div>
              ))}
          </div>
        </div>
        <div className="totalSummary">
          <p>Total (per {plan != null && plan.time.slice(0, plan.time.length - 2)})</p>
          <p className="feeTotal">
            ${total}/{time}
          </p>
        </div>
        <div className="btnGrup">
          <div className="btnGoBack">
            <p>Go Back</p>
          </div>
          <button className="btn" onClick={handleSubmit}>
            Confirm
          </button>
        </div>
      </div>
    </div>
  );
};

export default Summary;
